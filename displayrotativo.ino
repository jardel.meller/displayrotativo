#include <FastLED.h>

#define LED_PIN 5 //saida de dados da fita led
#define NUM_LEDS 52
#define brilho 255
#define tamVet 10
int pinoDigital = 2; //PINO DIGITAL (DETECTA A PRESENÇA DO CAMPO MAGNÉTICO)

int val = 0, flag = 0, grau = 0, voltas = 0;
boolean ligou = 0;
boolean acender = 0;
int antecipa = 0;
unsigned int passou, espera = 0;
unsigned long dif, tempo = 0, now, fim = 0, ini = 0;
unsigned long tempo_1grau = 100000000;
float k1 = 0.3, k2 = 0.7;
float rpm = 1;
int cent, dec, num;

/////////// Mesagens a serem escritas:
String mensagem1 = "Engenharia";
String mensagem2 = "Eletrica";
String mensagem3 = "2019  ";
////

const byte tam1 = mensagem1.length();
byte letras1[tamVet] = {0} ;
byte vet1[tamVet] = {0} ;

byte letras2[tamVet] = {0};
byte vet2[tamVet] = {0}; 
const byte tam2 = mensagem2.length();

byte letras3[tamVet] = {0}; // o q escrever
byte vet3[tamVet] = {0};
const byte tam3 = mensagem3.length();

byte tam4 = 7;
byte letras4[7] = {0, 0, 0, 0, 1, 2, 3}; // escreve "   RPM"
byte vet4[7] = {9, 17, 25, 33, 41, 49, 57}; // graus onde cada letra começa
boolean acabou1 = 0, acabou2 = 0, acabou3 = 0, acabou4 = 0;
int aux1 = 0, aux2 = 0, aux3 = 0, aux4 = 0;
int k, j = 0, m = 0, w, p, n, t = 0, y = 0;
int r1 = 255, g1 = 0, b1 = 0;
int r2 = 0, g2 = 255, b2 = 0;
int r3 = 0, g3 = 0, b3 = 255;
byte dado, velo;


const byte info[14][7][8] PROGMEM = {

  { //espaco - 0
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
  },
  { //R
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
  },
  { //P
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
  },
  { //M
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 1, 1, 0, 1, 0, 0},
    {1, 0, 1, 1, 0, 1, 0, 0},
    {1, 0, 1, 1, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
  },
  { //1
    {0, 0, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 0, 0, 0},
  },
  { //2
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  },
  { //3
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  },
  { //4
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
  },
  { //5
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 1, 1, 1, 1, 0, 0},
  },
  { //6
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  },
  { //7
    {0, 0, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
  },
  { //8
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  },
  { //9
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  },
  { //zero
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
  }

};

const byte alfabeto[37][9][8] PROGMEM = {

  { //espaco
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
  },
  { //A
    {0, 1, 1, 1, 1, 1, 0, 0},
    {1, 1, 0, 0, 0, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
  },
  { //B
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 1, 1, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 1, 1, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
  },
  { //C
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
  },
  { //D
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 1, 1, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //E
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
  },
  { //F
    {1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
  },
  { //G
    {1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
  },
  { //H
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
  },
  { //I
    {1, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
  },
  { //J
    {1, 1, 1, 1, 1, 1, 1, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 1, 1, 1, 0, 0, 0, 0},
  },
  { //K
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 1, 0, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 0, 1, 0, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
  },
  { //L
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //M
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 0, 0, 0, 1, 1, 0},
    {1, 0, 1, 0, 1, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
  },
  { //N
    {0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 0, 0, 0, 0, 1, 0},
    {1, 0, 1, 0, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 0, 1, 0, 1, 0},
    {1, 0, 0, 0, 0, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
  },
  { //O
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 1, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {0, 1, 0, 1, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
  },
  { //P
    {1, 1, 1, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 1, 1, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
  },
  { //Q
    {1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 1, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
  },
  { //R
    {1, 1, 1, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 1, 1, 1, 0, 0, 0, 0},
    {1, 1, 0, 0, 0, 0, 0, 0},
    {1, 0, 1, 0, 0, 0, 0, 0},
    {1, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
  },
  { //S
    {0, 1, 1, 1, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //T
    {1, 1, 1, 1, 1, 1, 1, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
  },
  { //U
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
  },
  { //V
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
  },
  { //W
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {1, 0, 0, 1, 0, 0, 1, 0},
    {0, 1, 0, 1, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
  },
  { //X
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 1, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
  },
  { //Y
    {0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
  },
  { //Z
    {1, 1, 1, 1, 1, 1, 1, 0},
    {0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 0},
  },
  { //1
    {1, 1, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //2
    {1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //3
    {1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //4
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
  },
  { //5
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //6
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //7
    {1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0},
  },
  { //8
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //9
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  },
  { //0
    {1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 1, 0, 0, 0},
    {1, 1, 1, 1, 1, 0, 0, 0},
  }
};

CRGB leds[NUM_LEDS];

void processa(String txt, byte letras[tamVet], byte vet[tamVet], byte tam )
{

  for (int p = 0; p < tam; p++)
  {
    if ( (txt[p] == 'A') || (txt[p] == 'a') )
    {
      letras[p] = 1;
      antecipa = 0;
    }
    else if ( (txt[p] == 'B') || (txt[p] == 'b') )
    {
      letras[p] = 2;
      antecipa = 0;
    }
    else if ( (txt[p] == 'C') || (txt[p] == 'c') )
    {
      letras[p] = 3;
      antecipa = 1;
    }
    else if ( (txt[p] == 'D') || (txt[p] == 'd') )
    {
      letras[p] = 4;
      antecipa = 0;
    }
    else if ( (txt[p] == 'E') || (txt[p] == 'e') )
    {
      letras[p] = 5;
      antecipa = 0;
    }
    else if ( (txt[p] == 'F') || (txt[p] == 'f') )
    {
      letras[p] = 6;
      antecipa = 0;
    }
    else if ( (txt[p] == 'G') || (txt[p] == 'g') )
    {
      letras[p] = 7;
      antecipa = 0;
    }
    else if ( (txt[p] == 'H') || (txt[p] == 'h') )
    {
      letras[p] = 8;
      antecipa = 0;
    }
    else if ( (txt[p] == 'I') || (txt[p] == 'i') )
    {
      letras[p] = 9;
      antecipa = 5;
    }
    else if ( (txt[p] == 'J') || (txt[p] == 'j') )
    {
      letras[p] = 10;
      antecipa = 0;
    }
    else if ( (txt[p] == 'K') || (txt[p] == 'k') )
    {
      letras[p] = 11;
      antecipa = 1;
    }
    else if ( (txt[p] == 'L') || (txt[p] == 'l') )
    {
      letras[p] = 12;
      antecipa = 2;
    }
    else if ( (txt[p] == 'M') || (txt[p] == 'm') )
    {
      letras[p] = 13;
      antecipa = 0;
    }
    else if ( (txt[p] == 'N') || (txt[p] == 'n') )
    {
      letras[p] = 14;
      antecipa = 0;
    }
    else if ( (txt[p] == 'O') || (txt[p] == 'o') )
    {
      letras[p] = 15;
      antecipa = 2;
    }
    else if ( (txt[p] == 'P') || (txt[p] == 'p') )
    {
      letras[p] = 16;
      antecipa = 3;
    }
    else if ( (txt[p] == 'Q') || (txt[p] == 'q') )
    {
      letras[p] = 17;
      antecipa = 0;
    }
    else if ( (txt[p] == 'R') || (txt[p] == 'r') )
    {
      letras[p] = 18;
      antecipa = 2;
    }
    else if ( (txt[p] == 'S') || (txt[p] == 's') )
    {
      letras[p] = 19;
      antecipa = 2;
    }
    else if ( (txt[p] == 'T') || (txt[p] == 't') )
    {
      letras[p] = 20;
      antecipa = 0;
    }
    else if ( (txt[p] == 'U') || (txt[p] == 'u') )
    {
      letras[p] = 21;
      antecipa = 0;
    }
    else if ( (txt[p] == 'V') || (txt[p] == 'v') )
    {
      letras[p] = 22;
      antecipa = 0;
    }
    else if ( (txt[p] == 'W') || (txt[p] == 'w') )
    {
      letras[p] = 23;
      antecipa = 0;
    }
    else if ( (txt[p] == 'X') || (txt[p] == 'x') )
    {
      letras[p] = 24;
      antecipa = 0;
    }
    else if ( (txt[p] == 'Y') || (txt[p] == 'y') )
    {
      letras[p] = 25;
      antecipa = 0;
    }
    else if ( (txt[p] == 'Z') || (txt[p] == 'z') )
    {
      letras[p] = 26;
      antecipa = 0;
    }
    else if  (txt[p] == '1')
    {
      letras[p] = 27;
      antecipa = 2;
    }
    else if  (txt[p] == '2')
    {
      letras[p] = 28;
      antecipa = 2;
    }
    else if  (txt[p] == '3')
    {
      letras[p] = 29;
      antecipa = 2;
    }
    else if  (txt[p] == '4')
    {
      letras[p] = 30;
      antecipa = 2;
    }
    else if  (txt[p] == '5')
    {
      letras[p] = 31;
      antecipa = 2;
    }
    else if  (txt[p] == '6')
    {
      letras[p] = 32;
      antecipa = 2;
    }
    else if  (txt[p] == '7')
    {
      letras[p] = 33;
      antecipa = 2;
    }
    else if  (txt[p] == '8')
    {
      letras[p] = 34;
      antecipa = 0;
    }
    else if  (txt[p] == '9')
    {
      letras[p] = 35;
      antecipa = 2;
    }
    else if  (txt[p] == '0')
    {
      letras[p] = 36;
      antecipa = 2;
    }
    else
    {
      letras[p] = 0;
      antecipa = 3;
    }

    if (tam > 5)
    {
      vet[0] = 1;
      if (p > 0)
      {
        vet[p] = vet[p - 1] + 8 - antecipa;
        antecipa = 0;
      }

    }

    else
    {
      vet[0] = 65 - (tam * 8);

      if (p > 0)
      {
        vet[p] = vet[p - 1] + 8 - antecipa;
        antecipa = 0;
      }
    }
  }
}


void velocidade(int pos, int var)
{

  switch (var) {

    case 0:
      letras4[pos] = 13;
      break;
    case 1:
      letras4[pos] = 4;
      break;
    case 2:
      letras4[pos] = 5;
      break;
    case 3:
      letras4[pos] = 6;
      break;
    case 4:
      letras4[pos] = 7;
      break;
    case 5:
      letras4[pos] = 8;
      break;
    case 6:
      letras4[pos] = 9;
      break;
    case 7:
      letras4[pos] = 10;
      break;
    case 8:
      letras4[pos] = 11;
      break;
    case 9:
      letras4[pos] = 12;
      break;

  }
}


void setup () {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(brilho);
  pinMode(13, OUTPUT);
  pinMode(pinoDigital, INPUT_PULLUP);

  processa(mensagem1, letras1, vet1, tam1);
  processa(mensagem2, letras2, vet2, tam2);
  processa(mensagem3, letras3, vet3, tam3);
}


void loop () {

  val = digitalRead(pinoDigital);
  ini = micros();

  if ( (grau >= vet1[aux1] && grau <= (vet1[aux1] + 7))    )
  {
    if (acabou1 == 0)
    {
      k = letras1[aux1];

      memcpy_P(&dado,  &alfabeto[k][0][j], sizeof dado);

      leds[51] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][1][j], sizeof dado);
      leds[50] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][2][j], sizeof dado);
      leds[49] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][3][j], sizeof dado);
      leds[48] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][4][j], sizeof dado);
      leds[47] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][5][j], sizeof dado);
      leds[46] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][6][j], sizeof dado);
      leds[45] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][7][j], sizeof dado);
      leds[44] = CRGB(r1 * dado , g1 * dado, b1 * dado);
      memcpy_P(&dado,  &alfabeto[k][8][j], sizeof dado);
      leds[43] = CRGB(r1 * dado , g1 * dado, b1 * dado);

      j++;
      acender = 1;
    }

    if   (grau == (vet1[aux1] + 7))
    {
      if (aux1 <= (tam1 - 1))
      {
        aux1++;
      }

      if ( aux1 == tam1 )
      {
        acabou1 = 1;
      }

      j = 0;
    }
  }


  if ((grau >= vet2[aux2] && grau <= (vet2[aux2] + 7)))
  {
    if ( (acabou2 == 0) )
    {
      w = letras2[aux2];

      memcpy_P(&dado,  &alfabeto[w][0][m], sizeof dado);
      leds[41] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][1][m], sizeof dado);
      leds[40] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][2][m], sizeof dado);
      leds[39] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][3][m], sizeof dado);
      leds[38] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][4][m], sizeof dado);
      leds[37] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][5][m], sizeof dado);
      leds[36] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][6][m], sizeof dado);
      leds[35] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][7][m], sizeof dado);
      leds[34] = CRGB(r2 * dado , g2 * dado, b2 * dado);
      memcpy_P(&dado,  &alfabeto[w][8][m], sizeof dado);
      leds[33] = CRGB(r2 * dado , g2 * dado, b2 * dado);

      acender = 1;
      m++;
    }
    if  (grau == (vet2[aux2] + 7))
    {

      if (aux2 <= (tam2 - 1))
      {
        aux2++;
      }

      if ( aux2 == tam2 )
      {
        acabou2 = 1;
      }
      m = 0;
    }
  }


  if ((grau >= vet3[aux3] && grau <= (vet3[aux3] + 7)))
  {
    if ( (acabou3 == 0) )
    {
      p = letras3[aux3];

      memcpy_P(&dado,  &alfabeto[p][0][n], sizeof dado);
      leds[31] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][1][n], sizeof dado);
      leds[30] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][2][n], sizeof dado);
      leds[29] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][3][n], sizeof dado);
      leds[28] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][4][n], sizeof dado);
      leds[27] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][5][n], sizeof dado);
      leds[26] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][6][n], sizeof dado);
      leds[25] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][7][n], sizeof dado);
      leds[24] = CRGB(r3 * dado , g3 * dado, b3 * dado);
      memcpy_P(&dado,  &alfabeto[p][8][n], sizeof dado);
      leds[23] = CRGB(r3 * dado , g3 * dado, b3 * dado);


      acender = 1;
      n++;
    }

    if  (grau == (vet3[aux3] + 7))
    {
      if (aux3 <= (tam3 - 1))
      {
        aux3++;
      }

      if ( aux3 == tam3)
      {
        acabou3 = 1;
      }
      n = 0;
    }
  }



  if ((grau >= vet4[aux4] && grau <= (vet4[aux4] + 7)))
  {
    if ( (acabou4 == 0) )
    {
      t = letras4[aux4];

      memcpy_P(&velo,  &info[t][0][y], sizeof velo);
      leds[20] = CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][1][y], sizeof velo);
      leds[19] =  CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][2][y], sizeof velo);
      leds[18] =  CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][3][y], sizeof velo);
      leds[17] =  CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][4][y], sizeof velo);
      leds[16] =  CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][5][y], sizeof velo);
      leds[15] =  CRGB(128 * velo , 0 * velo, 0 * velo);
      memcpy_P(&velo,  &info[t][6][y], sizeof velo);
      leds[14] =  CRGB(128 * velo , 0 * velo, 0 * velo);

      acender = 1;
      y++;
    }

    if  (grau == (vet4[aux4] + 7))
    {

      if (aux4 <= (tam4 - 1))
      {
        aux4++;
      }

      if ( aux4 == tam4)
      {
        acabou4 = 1;
      }
      y = 0;
    }
  }

  if ( acender == 1)
  {
    FastLED.show();
    ligou = 1;
    acender = 0;
  }


  /////////--------------------------------------
  if (val == 0)  //Campo detectado
  {
    if (flag == 0)
    {

      flag = 1;

      grau = 0;
      aux1 = 0;
      aux2 = 0;
      aux3 = 0;
      aux4 = 0;
      acabou1 = 0;
      acabou2 = 0;
      acabou3 = 0;
      acabou4 = 0;

      voltas++;
      if (voltas == 100) // Define apos quantas voltas o sistema mudará de cor
      {
        r1 = random(0, 255);
        g1 = random(0, 255);
        b1 = random(0, 255);

        r2 = random(0, 255);
        g2 = random(0, 255);
        b2 = random(0, 255);

        r3 = random(0, 255);
        g3 = random(0, 255);
        b3 = random(0, 255);
        voltas = 0;
      }

      //////// calculo tempo:
      now = micros();
      dif = (now - tempo) * k1 + dif * k2 ;  // esse calculo serve para evitar pequenas oscilações causadas pela mudança de velocidade
      tempo = now;
      tempo_1grau = dif / 360 ;

      //// calcula rpm
      rpm = 60 / ((float)dif * pow(10, -6));
      cent = rpm / 100;
      dec = (rpm / 100 - cent) * 10;
      num = int(rpm) % 10 ;
      velocidade(0, cent);
      velocidade(1, dec);
      velocidade(2, num);
      /////
      if (tempo_1grau < 65530)
      {
        espera = (unsigned int)tempo_1grau;
      }
    }

  }
  if (val == 1)
  {
    flag = 0;
  }

  fim =  micros() - ini;
  passou = (unsigned int)fim;

  if (ligou == 0)
  {
    delayMicroseconds(espera * 0.92 - passou);
  }
  if (ligou == 1)
  {
    ligou = 0;
  }

  grau++;
}